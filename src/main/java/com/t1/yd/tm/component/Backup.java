package com.t1.yd.tm.component;

import com.t1.yd.tm.command.data.AbstractDataCommand;
import com.t1.yd.tm.command.data.DataBackupLoadCommand;
import com.t1.yd.tm.command.data.DataBackupSaveCommand;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

}
