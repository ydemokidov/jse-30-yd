package com.t1.yd.tm.command.user;

import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String name = "user_update_profile";

    @NotNull
    private final String description = "Update user profile";

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");

        @NotNull final String userId = getAuthService().getUserId();

        System.out.println("[ENTER LAST NAME:]");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER FIRST NAME:]");
        @NotNull final String fstName = TerminalUtil.nextLine();
        System.out.println("[ENTER MID NAME:]");
        @NotNull final String midName = TerminalUtil.nextLine();

        getUserService().updateUser(userId, fstName, lastName, midName);

        System.out.println("[USER PROFILE UPDATED]");
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
