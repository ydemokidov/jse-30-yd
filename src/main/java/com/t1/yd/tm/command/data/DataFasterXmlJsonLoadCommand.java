package com.t1.yd.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataFasterXmlJsonLoadCommand extends AbstractFasterXmlLoadCommand {

    @NotNull
    private final String name = "load_fasterxml_json";

    @NotNull
    private final String description = "Load JSON with FasterXml library";

    @Override
    @NotNull
    protected ObjectMapper getMapper() {
        return new ObjectMapper();
    }

    @Override
    @NotNull
    protected String getFile() {
        return FILE_JSON;
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
