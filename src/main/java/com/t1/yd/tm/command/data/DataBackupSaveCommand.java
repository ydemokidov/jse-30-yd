package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.Domain;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public final static String NAME = "save_backup";

    @NotNull
    public final String description = "Save backup to file";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BACKUP);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[BACKUP DATA SAVED]");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @NotNull
    public String getDescription() {
        return description;
    }

}
